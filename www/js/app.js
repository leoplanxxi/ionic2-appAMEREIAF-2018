
angular.module('app', ['ionic','ionic.service.core', 'app.controllers', 'app.routes', 'app.services', 'app.directives','ngCordova','ui.bootstrap','monospaced.qrcode','ngSanitize','ionic-ratings'])

.run(function($rootScope,$ionicPlatform,$cordovaSQLite) {


  $ionicPlatform.ready(function() {
    var notificationOpenedCallback = function(jsonData) {
    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };
  /*window.plugins.OneSignal.init("ace409fa-4b9c-4d1c-a570-52907aa366ee",
                                 {googleProjectNumber: "911801797370"},
                                 notificationOpenedCallback);
  window.plugins.OneSignal.enableInAppAlertNotification(true);*/

  window.plugins.OneSignal
 .startInit("ace409fa-4b9c-4d1c-a570-52907aa366ee")
 .handleNotificationOpened(notificationOpenedCallback)
 .endInit();
 cordova.plugins.locationAccuracy.canRequest(function(canRequest){
     if(canRequest){
         cordova.plugins.locationAccuracy.request(function(){
             console.log("Request successful");
         }, function (error){
             console.error("Request failed");
             if(error){
                 // Android only
                 console.error("error code="+error.code+"; error message="+error.message);
                 if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                     if(window.confirm("Fallo la activación automatica de Localización. ¿Desea activar la localización de manera Manual?")){
                         cordova.plugins.diagnostic.switchToLocationSettings();
                     }
                 }
             }
         }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
         );
     }
 });
   if (window.cordova && window.cordova.plugins.Keyboard) {
     cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
     cordova.plugins.Keyboard.disableScroll(true);

   }

   if (window.StatusBar) {
     // org.apache.cordova.statusbar required
     StatusBar.styleDefault();
   }
   if(window.navigator && window.navigator.splashscreen) {
   //window.plugins.orientationLock.unlock();
   }
dbFavoritos = $cordovaSQLite.openDB("favoritos.db");
$cordovaSQLite.execute(dbFavoritos, "CREATE TABLE IF NOT EXISTS favoritos (id integer primary key, accion text, ponente text, hora text, lugar text,indice integer,id_event text)");
});

 dbFavoritos = window.openDatabase("favoritos.db", '1', 'favoritos', 1024 * 1024 * 100); // browserindex_element
 $cordovaSQLite.execute(dbFavoritos, "CREATE TABLE IF NOT EXISTS favoritos (id integer primary key, accion text, ponente text, hora text, lugar text,indice integer,id_event text)");
});
