angular.module('app.directives', [])


.directive('map1', function() {
  //$scope.chat = Chats.get($stateParams.chatId);
  return {
    restrict: 'E',
    scope: {
    onCreate: '&'
    },

    link: function ($scope, $element, $attr) {

      function initialize() {

		mensaje();
      }


      function mensaje(){

        //var site = new google.maps.LatLng(19.0248424,-98.2460571);
        var site = new google.maps.LatLng(18.6445928,-91.8191971);
        var mapOptions = {

          center: site,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        //mapOptions.center = google.maps.LatLng(18.9989099,-98.2014037);
        var map = new google.maps.Map($element[0], mapOptions);

         var marker = new google.maps.Marker({
          position: site,
          map: map,
          title: 'Amereiaf'
        });


        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });
      }

      if (document.readyState === "complete") {

        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }

})

.directive('ngModelOnblur', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1,
        link: function(scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox' || attr.type === 'input') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
})

.directive('map2', function() {
  //$scope.chat = Chats.get($stateParams.chatId);
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'

    },

    link: function ($scope, $element, $attr) {

      function initialize() {

		mensaje();
      }


      function mensaje(){

        var site = new google.maps.LatLng(18.6404419,-91.8230076);
        var site2 = new google.maps.LatLng(18.6404419,-91.8230076);
        var mapOptions = {

          center: site,
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //mapOptions.center = google.maps.LatLng(18.9989099,-98.2014037);
        var map = new google.maps.Map($element[0], mapOptions);

         var marker = new google.maps.Marker({
          position: site2,
          map: map,
          title: 'Simposio Buap'
        });


        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });
      }

      if (document.readyState === "complete") {

        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
})

.directive('goNative', ['$ionicGesture', '$ionicPlatform', function($ionicGesture, $ionicPlatform) {
  return {
    restrict: 'A',

    link: function(scope, element, attrs) {

      $ionicGesture.on('tap', function(e) {

        var direction = attrs.direction;
        var transitiontype = attrs.transitiontype;

        $ionicPlatform.ready(function() {

          switch (transitiontype) {
            case "slide":

              window.plugins.nativepagetransitions.slide({
                  "direction": direction
                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
              break;
            case "flip":
              window.plugins.nativepagetransitions.flip({
                  "direction": direction
                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
              break;

            case "fade":
              window.plugins.nativepagetransitions.fade({

                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
              break;

            case "drawer":
              window.plugins.nativepagetransitions.drawer({
          "origin"         : direction,
          "action"         : "open"
                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
              break;

            case "curl":
              window.plugins.nativepagetransitions.curl({
          "direction": direction
                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
              break;

            default:
              window.plugins.nativepagetransitions.slide({
                  "direction": direction
                },
                function(msg) {
                  console.log("success: " + msg)
                },
                function(msg) {
                  alert("error: " + msg)
                }
              );
          }


        });
      }, element);
    }
  };
}]);
