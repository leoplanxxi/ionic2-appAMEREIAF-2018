angular.module('app.services', [])

.factory('amereiaf',function($http){
    var remote = 'http://148.228.54.18/app-amereiaf/api/';
    var otros  = 'http://148.228.54.18/app-amereiaf/api/'
    var items =[];
    return{
      dia: function(dia){
        return $.post(remote+'progxdia/',dia).success(function(items){
          return items;
        })
      },

      aviso:function(){
        return $.post(remote+'avisos').success(function(item){
          return item;
        })
      },

      Obtenerinfo:function(){
        return $http.get(otros+'info').then(function(response){
          items=response.data;
          return items;
        })
      },

      Obtenerinfoc:function(){
        return $http.get(otros+'infoc').then(function(response){
          items=response.data;
          return items;
        })
      },

      Obtenersede:function(){
        return $http.get(otros+'sede').then(function(response){
          items=response.data;
          return items;
        })
      },

      Obtenerponentes:function(){
        return $http.get(otros+'ponentes').then(function(response){
          items=response.data;
          return items;
        })
      },
      Ubicaciones: function(){
      return $http.get(remote+'ubicaciones').then(function(response){
        return response.data;
      })
    },
    Estado: function(){
    return $http.get(remote+'estado').then(function(response){
      return response.data;
    })
  },
      Obtenerponente:function(idp){
        return $http.get(otros+'idponente/'+idp).then(function(response){
          items=response.data;
          return items;
        })
      },
    }
})

.service('Navigation', function($state) {
  //directly binding events to this context
  this.goNative = function(view, data, direction) {
    $state.go(view, data);
    window.plugins.nativepagetransitions.slide({
        "direction": direction
      },
      function(msg) {
        //console.log("success: " + msg)
      }, // called when the animation has finished
      function(msg) {
        alert("error: " + msg)
      } // called in case you pass in weird values
    );
  };
});
