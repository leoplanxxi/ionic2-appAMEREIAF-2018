﻿angular.module('app.controllers', ['ionic','ngCordova'])


.controller('menuCtrl', function($scope) {

})

.controller('simposioBuapCtrl', function($scope,$cordovaGeolocation,$location,$state,amereiaf) {
  window.localStorage.removeItem('dia1');
  window.localStorage.removeItem('dia2');
  window.localStorage.removeItem('dia3');

amereiaf.Estado().then(function(item){
  if(item[0]['estado_app'] == 0){
    $state.go('menu.temporal');
  }
});

})

.controller('temporalCtrl',function($scope,$rootScope,$ionicHistory,$ionicSideMenuDelegate,amereiaf){
  $ionicHistory.clearCache();
  $ionicHistory.clearHistory();
  $ionicSideMenuDelegate.canDragContent(false);
  amereiaf.Estado().then(function(item){
    $scope.estado = item;
  });
})
.controller('LunesCtrl', function($scope,$rootScope,$location,$stateParams,$state,$ionicLoading,$cordovaSQLite,$cordovaToast,$ionicModal,amereiaf,$ionicLoading){

switch ($stateParams.dia) {
  case "Jueves":
    $scope.programa=[];

      if(window.localStorage['dia1']==null){
        $ionicLoading.show({
          template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
          duration: 3000
        })
        var int= setInterval(Revision,1000);
        function Revision(){
           amereiaf.dia($stateParams).success(function(item){
                if (item==null){
                  alert("Error de red");
                }
                $scope.programa = item;
                window.localStorage['dia1'] = JSON.stringify($scope.programa);
            });
           if(window.localStorage['dia1']!=null){
              clearInterval(int);
           }
        }
        $state.reload();
      }else{
        $scope.programa = JSON.parse(window.localStorage['dia1']);
      }

    break;
  case "Viernes":
  $scope.programa=[];
  if(window.localStorage['dia2']==null){
    $ionicLoading.show({
        template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
          duration: 3000
        })
      amereiaf.dia($stateParams).then(function(item){
      $scope.programa = item;
      window.localStorage['dia2'] = JSON.stringify($scope.programa);;

    });

  }else{
    $scope.programa = JSON.parse(window.localStorage['dia2']);
  }
    break;
  case "Miércoles":
  $scope.programa=[];
  if(window.localStorage['dia3']==null){
    $ionicLoading.show({
        template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
          duration: 3000
        })
    amereiaf.dia($stateParams).then(function(item){
      $scope.programa = item;
      window.localStorage['dia3'] = JSON.stringify($scope.programa);;
    });

  }else{
    $scope.programa = JSON.parse(window.localStorage['dia3']);

  }
    break;
}




   $scope.colores=function(val){
        if(val==1){
        $scope.modal2.show();
      }
      else{console.log("error");}
      };
    $scope.cerrar = function(){
      $scope.modal2.hide();
    };

    $ionicModal.fromTemplateUrl('codigo.html', function($ionicModal){
      $scope.modal2= $ionicModal;
    }, {
       scope: $scope
    });


 $scope.dia=$stateParams.dia;

  $scope.datos=[];
  $scope.ponente=[];
  $scope.$on('refrescar',function(event,news){

  $scope.$apply(function(){

  $scope.datos=news;

    for(i=0; i<$scope.datos.length; i++){
    $scope.datos[i].index=i;
    $scope.datos[i].rate = 0;
    $scope.datos[i].doc.rate = 0;
        var queryCtrl = "SELECT indice FROM favoritos WHERE accion=?";
        $cordovaSQLite.execute(dbFavoritos, queryCtrl,[$scope.datos[i].accion.a1]).then(function(res){
        if(res.rows.length > 0){
          $state.reload();
          $state.go($state.current, {}, {reload: true});
          $scope.datos[res.rows.item(0).indice].doc.rate = 1;}
      });
  }
 })
    })

	 $scope.insertFav = function(accion, ponente, hora, lugar,indiceE,id_event) {
var queryRep = "SELECT accion,indice,id FROM favoritos WHERE accion = ?";
  $cordovaSQLite.execute(dbFavoritos, queryRep,[accion]).then(function(res){
    if(res.rows.length > 0) {
      idaux = res.rows.item(0).id;
     $scope.datos[res.rows.item(0).indice].rate=0;

      var query = "DELETE FROM favoritos WHERE id = ?";
      $cordovaSQLite.execute(dbFavoritos, query,[idaux]).then(function(res) {
      $cordovaToast.showLongBottom('Se elimino de favoritos').then(function(success) { }, function (error) {})
      }, function (err) {
        console.error(err);
      });
      }else{
          var query = "INSERT INTO favoritos (accion, ponente, hora, lugar,indice,id_event) VALUES (?,?,?,?,?,?)";
          $cordovaSQLite.execute(dbFavoritos, query, [accion, ponente, hora, lugar,indiceE,id_event]).then(function(res) {
         $scope.datos[indiceE].rate=1;
        $cordovaToast.showLongBottom('Se agrego a favoritos').then(function(success) { }, function (error) {})
        }, function (err) {
          console.error(err);
        });
    }
  }, function(err){
    console.error(err);
  });
}

})


.controller('ponentesCtrl', function($scope,$rootScope,$stateParams,$ionicLoading,amereiaf) {
  $ionicLoading.show({
      template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
        duration: 3000
      })
  $scope.ponente=[];
  amereiaf.Obtenerponentes().then(function(items){
    $scope.ponente=items;
  })
})

.controller('personaCtrl' , function($scope,$rootScope, $stateParams, $ionicModal,amereiaf){
var idp = $stateParams.idp;
$rootScope.nombre=[];
$rootScope.deta=[];
$rootScope.img=[];

amereiaf.Obtenerponente(idp).then(function(items){
  $rootScope.nombre=items[0].nombre;
  $rootScope.deta=items[0].descripcion;
  $rootScope.img=items[0].imagen;
})
})


.controller('sedetemCtrl' , function($scope,$rootScope, $stateParams, $ionicModal,amereiaf){

var idp = $stateParams.idp;
$rootScope.nombre=[];
$rootScope.deta=[];
$rootScope.img=[];

amereiaf.Obtenersede().then(function(items){
  for(var i=0; i<items.length;i++){
    if(idp == items[i].id_sede){
      $rootScope.nombre=items[i].titulo;
      $rootScope.deta=items[i].descripcion;
      $rootScope.img=items[i].imagen;
    }
  }


})
})

.controller('ubicacionCtrl',function($scope,$rootScope,$cordovaGeolocation,$ionicPopover,amereiaf,$ionicModal,$interval,$ionicLoading){
  $ionicLoading.show({
      template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
        duration: 3000
      })
  $scope.ubicaciones=[];
  $scope.items=[];

  var map = null;
  var directionsDisplay = null;
  var directionsService = null;
  var tipom = '';
  $scope.model = "";
  var inicio="";
  var fin="";
  var tipov="";
  //var tipom="";//tipo de kml a cargar
  $scope.clickedValueModel = "";
  $scope.removedValueModel = "";
  $scope.tipo=[
    {valor:"DRIVING",nombre:"En Auto"}
  ]

  $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
    $scope.popover = popover;
  });

  amereiaf.Ubicaciones().then(function(items){
    $scope.ubicaciones = items;
    for(var i=0;i<$scope.ubicaciones.length;i++){
      $scope.items.push({
        id:$scope.ubicaciones[i].latitude+','+$scope.ubicaciones[i].longitud,
        name:$scope.ubicaciones[i].lugar,
        view:$scope.ubicaciones[i].lugar
      });
    }
  });

  $ionicModal.fromTemplateUrl('templates/origen.html', {
  id: '1', // We need to use and ID to identify the modal that is firing the event!
  scope: $scope,
  backdropClickToClose: false,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.oModal1 = modal;
});

// Modal 2
$ionicModal.fromTemplateUrl('templates/destino.html', {
  id: '2', // We need to use and ID to identify the modal that is firing the event!
  scope: $scope,
  backdropClickToClose: false,
  animation: 'slide-in-up'
}).then(function(modal) {
  $scope.oModal2 = modal;
});

$scope.openModal = function(index) {

  switch (index) {
    case 1:
      $scope.oModal1.show();
      break;
    case 2:
      $scope.oModal2.show();
      break;
    case 3:
      $scope.oModal3.show();
      break;
    case 4:
      $scope.oModal4.show();
      break;
  }

};

$scope.closeModal = function(index) {

  switch (index) {
    case 1:
      $scope.oModal1.hide();
      break;
    case 2:
      $scope.oModal2.hide();
      break;
    case 3:
      $scope.oModal3.hide();
      break;
    case 4:
      $scope.oModal4.hide();
      break;
    }

};

$scope.$on('$destroy', function() {
  $scope.oModal1.remove();
  $scope.oModal2.remove();
  $scope.oModal3.remove();
  $scope.oModal4.remove();
});

$scope.origen=function(data){
  inicio=data;

  $scope.closeModal(1);
}

$scope.destino=function(data){
  fin=data;
  $scope.closeModal(2);
}

$scope.tipo_v=function(data){
  tipov=data;
  $scope.closeModal(3);
}



  $scope.miubicacion="";
  $scope.posicion="0,0";

  $scope.miposicion = function(mipos){
    //$scope.mplat=lat;
    //$scope.mplong=long;
    $scope.posicion=mipos;
    pintarmapa();
  }


  function pintarmapa(){
  var posOptions = {maximumAge: 0, timeout: 50000, enableHighAccuracy:true};
  $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
      var lat  = position.coords.latitude
      var long = position.coords.longitude
      $scope.miubicacion = lat+","+long;
      $interval(function(){
      //getMap(lat,long);
      },6000);
      getMap(lat, long);

    }, function(err) {
      console.log(err);
    });

  var onMapSuccess = function (position) {
      Latitude = position.coords.latitude;
      Longitude = position.coords.longitude;
      getMap(Latitude, Longitude);
  }

  // Get map by using coordinates

  function getMap(latitude, longitude) {

      var mapOptions = {
          center: new google.maps.LatLng($scope.posicion),
          zoom: 2,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map
      (window.document.getElementById("map"), mapOptions);

      var latLong = new google.maps.LatLng(latitude, longitude);
      //var myLatlng = new google.maps.LatLng(latitude, longitude);

		directionsDisplay = new google.maps.DirectionsRenderer();
		directionsService = new google.maps.DirectionsService();

      var marker = new google.maps.Marker({
          position: latLong
      });

      marker.setMap(map);
      map.setZoom(16);
      map.setCenter(marker.getPosition());
      /*Inicio de la función que carga el KML*/
      loadKmlLayer(map);
  }

  /*Inicio de la incorporación del KML*/
  function loadKmlLayer(map) {
        var kmlLayer = new google.maps.KmlLayer(tipom, {
          suppressInfoWindows: true,
          preserveViewport: false,
          map: map
        });

      var capure = new  google.maps.event.addListener(kmlLayer, 'click', function(event) {

          $scope.popover.show();
          document.body.classList.remove('platform-ios');
          document.body.classList.remove('platform-android');
          document.body.classList.add('platform-ionic');

          var content = event.featureData.infoWindowHtml;
          var testimonial = document.getElementById('capture');
          testimonial.innerHTML = content;
        });
      }
      /*Final de la incorporación del KML*/



  $scope.getDirections = function(){

    var start=inicio;
    var end=fin;
		if(!start || !end){
			alert("Origen,Destino");
			return;
		}
		var request = {
		        origin: start,
		        destination: end,
		        travelMode: google.maps.DirectionsTravelMode["DRIVING"],
		        unitSystem: google.maps.DirectionsUnitSystem["METRIC"],
		        provideRouteAlternatives: true
	    };
		directionsService.route(request, function(response, status) {
	        if (status == google.maps.DirectionsStatus.OK) {
	            directionsDisplay.setMap(map);
	            directionsDisplay.setPanel($("#directions_panel").get(0));
	            directionsDisplay.setDirections(response);
	        } else {
	            alert("Por ahora no se puede mostrar la ruta, intentelo más tarde.");
	        }
	    });
	}
  // Success callback for watching your changing position

  var onMapWatchSuccess = function (position) {

      var updatedLatitude = position.coords.latitude;
      var updatedLongitude = position.coords.longitude;

      if (updatedLatitude != Latitude && updatedLongitude != Longitude) {

          Latitude = updatedLatitude;
          Longitude = updatedLongitude;

          getMap(updatedLatitude, updatedLongitude);
      }
  }

  // Error callback

  function onMapError(error) {
      console.log('code: ' + error.code + '\n' +
          'message: ' + error.message + '\n');
  }

  // Watch your changing position

  function watchMapPosition() {

      return navigator.geolocation.watchPosition
      (onMapWatchSuccess, onMapError, { enableHighAccuracy: true });
  }
}

pintarmapa();
})

.controller('buzonCtrl', function($scope, $http, $cordovaToast, $location, $ionicLoading) {

    $scope.submit = function(nombre, email, mensaje) {
        //alert("Soy el usuario "+ nombre+" "+email+" "+asunto+" "+mensaje);
        $.post("http://148.228.54.18/app-amereiaf/comentarios/crear",{'nombre':nombre,'correo':email,'comentario':mensaje})
        .success(function(data){

            $cordovaToast.showLongBottom('Se envío el mensaje').then(function(success) {
            // success

        }, function (error) {
    console.log(error);
        })

    })
        $location.path('#/menu/page5');
    }
    $ionicLoading.hide({});
})


.controller('patrocinadoresCtrl', function($scope) {


})

  //controlador de las noticias, que hace conexion a la base de datos.

.controller('noticiasCtrl', function($scope,$stateParams,$ionicLoading,amereiaf) {
    $ionicLoading.show({
    template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
    duration: 3000
  });

    $scope.noticias=[];

    amereiaf.aviso().then(function(item){
      if (item == null ) alert("Error de conexion");
      $scope.noticias = item;
    });
    ///////////////////////////////////////////////////////////////////////
    //$ionicLoading.hide();

  })


.controller('gafeteCtrl', function($scope) {
  if(window.localStorage['gafete']==null){
    //window.localStorage['gafete'] = "AMEREIAF";
  }

  $scope.foo = window.localStorage['gafete'];
  $scope.gafete = function(id){
    window.localStorage['gafete'] = id;
  }



})

.controller('favoritosCtrl', function($rootScope,$scope,$cordovaSQLite,$state,$location,$cordovaToast) {
       //$state.reload();
       //$state.go($state.current, {}, {reload: true});
      $scope.$on('$ionicView.beforeEnter', function() {
      $scope.oculto = false;
      var query = "SELECT * FROM favoritos";
      $scope.saved = [];
      $cordovaSQLite.execute(dbFavoritos, query).then(function(res) {
            if(res.rows.length > 0 ) {
        for(var i = 0; i < res.rows.length; i++) {
            if($scope.saved.id!=res.rows.item(i).id){
                  $scope.saved.push({id: res.rows.item(i).id,
                  accionFav: res.rows.item(i).accion,
                  ponenteFav: res.rows.item(i).ponente,
                  horaFav: res.rows.item(i).hora,
                  lugarFav: res.rows.item(i).lugar,
                  diaFav: res.rows.item(i).id_event,
                  index: res.rows.item(i).indice
                  });
                  }
          $scope.oculto = true;
                }

      } else {
                $scope.oculto = false;
            }
        }, function (err) {
            console.error(err);
        });
      //})

   $scope.deleteFav = function(id,indexE) {
      var indexObj = "SELECT * FROM favoritos WHERE id = ?";
      $cordovaSQLite.execute(dbFavoritos, indexObj,[id]).then(function(res){

          var query = "DELETE FROM favoritos WHERE id = ?";

          $cordovaSQLite.execute(dbFavoritos, query,[id]).then(function(res) {

          $state.go($state.current, {}, {reload: true});
          $state.reload();
          //$location.path('#/menu/page10');
          $cordovaToast.showLongBottom('Se elimino de favoritos').then(function(success) {}, function (error) {})

          }, function (err) {
            console.error(err);
          });

        },function (err) {
              console.error(err);
          });
      $state.reload();
      $state.go($state.current, {}, {reload: true});
    }
      //
    })

    $scope.doRefresh = function($cordovaSQLite) {
      $scope.$broadcast('scroll.refreshComplete');
    }

})


.controller('amereCtrl',function($scope,$rootScope,$stateParams,$ionicLoading,amereiaf){
  $ionicLoading.show({
      template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
        duration: 3000
      })
  $scope.detalles=[];
  $scope.detallesc=[];
  amereiaf.Obtenerinfo().then(function(items){
    $scope.detalles=items;
  })
  amereiaf.Obtenerinfoc().then(function(items){
    $scope.detallesc=items;
  })

})

.controller('sedeCtrl',function($scope,$rootScope,$stateParams,$ionicLoading,amereiaf){
  $ionicLoading.show({
      template: '<ion-spinner icon="spiral" class="spinner-calm"></ion-spinner>',
        duration: 3000
      })
  $scope.detalles=[];
  amereiaf.Obtenersede().then(function(items){
    $scope.detalles=items;
  })
})
.controller('valCtrl',function($scope,$cordovaToast,$stateParams,amereiaf,$location,$ionicViewService,$ionicHistory){

  $scope.algomas="";
  $scope.ver = true;
  var remote = 'http://148.228.54.18/app-amereiaf/api/';

  var id = $stateParams.id_prog;
  $.post(remote+'fecha',{'id':id}).success(function(item){

    if(item == 'error' && window.localStorage[id+'env'] == null){
      $scope.ver = false;
      $cordovaToast.showLongBottom('Aún no pudes evaluar este evento.').then(function(success) {}, function (error) {});
      $ionicViewService.getBackView().go();
      //  $ionicHistory.clearHistory();
      //window.location.reload();
    }else{
      $scope.ver = true;
    }
  });

  var read = false;
  $scope.mens = "Gracias por su valoración";
  $scope.ment = false;
  $scope.ratingsObject = {
        iconOn: 'ion-ios-star',    //Optional
        iconOff: 'ion-ios-star-outline',   //Optional
        iconOnColor: 'rgb(223, 181, 93)',  //Optional
        iconOffColor:  'rgb(223, 181, 93)',    //Optional
        rating:  window.localStorage[id+'v'], //Optional
        minRating:1,    //Optional
        readOnly: read, //Optional
        callback: function(rating, index) {    //Mandatory
          $scope.ratingsCallback(rating, index);
        }
      };

  if(window.localStorage[id+'env']==null){
    read = false;
    window.localStorage[id+'v']=1;
  }else{
    read = true;
    $scope.ment = true;
  }

  $scope.ratingsObject = {
        iconOn: 'ion-ios-star',    //Optional
        iconOff: 'ion-ios-star-outline',   //Optional
        iconOnColor: 'rgb(223, 181, 93)',  //Optional
        iconOffColor:  'rgb(223, 181, 93)',    //Optional
        rating:  window.localStorage[id+'v'], //Optional
        minRating:1,    //Optional
        readOnly: read, //Optional
        callback: function(rating, index) {    //Mandatory
          $scope.ratingsCallback(rating, index);
        }
      };

    $scope.ratingsCallback = function(rating, index) {
        window.localStorage[id+'rating'] = rating;
      };

      $scope.enviar = function(am){

        window.localStorage[id+'env']=true;
        window.localStorage[id+'v']=window.localStorage[id+'rating'];

        $.post(remote+'val/',{'id':id,'rating':window.localStorage[id+'rating'],'comentario':am}).success(function(items){

          $cordovaToast.showLongBottom('Gracias por su valoración').then(function(success) {}, function (error) {});
          $ionicViewService.getBackView().go();
          //$ionicHistory.clearHistory();
          //window.location.reload();
        })

      }





})
