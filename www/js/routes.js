angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.views.transition('none');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
    })

    .state('menu.simposioBuap', {
      url: '/page1',
      views: {
        'menus': {
        templateUrl: 'templates/simposioBuap.html',
        controller: 'simposioBuapCtrl'
        }
      }
    })


    .state('menu.programa', {
      url: '/page2',
      views: {
        'menus': {
      templateUrl: 'templates/programa.html'
      }
        }
    })

    .state('menu.ponentes', {
      url: '/page3',
        views: {
        'menus': {
      templateUrl: 'templates/ponentes.html',
      controller: 'ponentesCtrl'
        }
          }
    })

    .state('menu.ubicacion', {
      url: '/page4',
        views: {
        'menus': {
      templateUrl: 'templates/ubicacion2.html',
      controller: 'ubicacionCtrl'
        }
          }
    })


    .state('menu.buzon', {
      url: '/page5',
        views: {
        'menus': {
      templateUrl: 'templates/buzon.html',
      controller: 'buzonCtrl'
        }
          }
    })

    .state('menu.patrocinadores', {
      url: '/page6',
        views: {
        'menus': {
      templateUrl: 'templates/patrocinadores.html',
      controller: 'patrocinadoresCtrl'
        }
          }
    })

    .state('menu.noticias', {
      url: '/page7/:noti',
        views: {

        'menus': {
      templateUrl: 'templates/noticias.html',
      controller: 'noticiasCtrl'
        }
          }
    })

.state('menu.gafete', {
      url: '/page8',
      views: {
        'menus': {
          templateUrl: 'templates/gafete.html',
          controller: 'gafeteCtrl'
        }
      }
    })

    .state('menu.Lunes',{
      url: '/page9/:dia',
      views:{
        'menus':{
      templateUrl:'templates/Lunes.html',
      controller: 'LunesCtrl'
       }
      }
    })


    .state('menu.favoritos',{
      url: '/page10',
      views:{
        'menus':{
          templateUrl:'templates/favoritos.html',
          controller:'favoritosCtrl'
        }
      }
    })

    .state('menu.detail',{
      url: '/page11',
      views:{
        'menus':{
          templateUrl:'templates/detail.html'
        }
      }
    })
    .state('menu.persona',{
      url: '/page12/:idp',
      views:{
        'menus':{
          templateUrl:'templates/persona.html',
          controller: "personaCtrl"
        }
      }
    })

    .state('menu.sedetem',{
      url: '/page13/:idp',
      views:{
        'menus':{
          templateUrl:'templates/sedetem.html',
          controller: "sedetemCtrl"
        }
      }
    })


    .state('menu.amereiaf',{
      url: '/amereiaf',
      views:{
        'menus':{
          templateUrl:'templates/amereiaf.html',
          controller: 'amereCtrl'
        }
      }
    })
    .state('menu.sede',{
      url: '/sede',
      views:{
        'menus':{
          templateUrl:'templates/sede.html',
          controller: 'sedeCtrl'
        }
      }
    })
    .state('menu.valoracion',{
      url:'/valoracion/:id_prog',
      views:{
        'menus':{
          templateUrl:'templates/valoracion.html',
          controller:'valCtrl'
        }
      }
    })
    .state('menu.temporal',{
      url:'/temporal',
      views:{
        'menus':{
          templateUrl:'templates/temporal.html',
          controller:'temporalCtrl',
          disableHardwareBackButton : true
        }
      }
    })

  // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/menu/page1');
});
